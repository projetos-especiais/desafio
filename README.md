# Desafio

O desafio consiste em criar uma solução para gestão de projetos de software.
Precisamos de um sistema Web que controle os projetos que são demandados pelas áreas de negócio.
 
Todo **Projeto** possui:
 
* Nome
* Descrição
* Lista de requisitos
* Data de início
* Data de fim
* Entregas parciais com Data do deploy e Lista de Requisitos
* Período de homologação das entregas (data de início e fim)
* Lista de Desenvolvedores alocados
 
## Tecnologias que devem ser utilizadas para a implementação do desafio:
* Linguagem: Java 8
* Aplicação Web : Angular 7+
* API: Spring Boot 2.x
* Build: Maven ou Gradle
* Banco de dados Oracle ou MySQL
* Testes com JUnit/Mockito
 
Quer melhorar? Você ainda pode usar:
* Containers Docker
* Autenticação com JWT
* Flyway
* Lombok
 
> O tempo está apertado? Pode focar somente na construção da API :)
 
Entregue o código-fonte em um repositório git.
